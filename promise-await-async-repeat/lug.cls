\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{lug}[2017/10/18]

\LoadClass{beamer}

\usetheme[numbering=none,progressbar=frametitle,block=fill]{metropolis}
\setbeamercovered{dynamic}
\RequirePackage{graphicx}

\RequirePackage{ifxetex}
\ifxetex\RequirePackage{fontspec}\fi

\RequirePackage{minted}
\RequirePackage{xcolor}
\RequirePackage{hyperref}

\renewcommand*\footnoterule{}
\setminted{autogobble,python3,mathescape}

\beamertemplatenavigationsymbolsempty%
\def\logoimage{graphics/lug}

\setlength\parindent{0pt}

\AtBeginDocument{%
    \begin{frame}
        \maketitle
    \end{frame}
}

\AtEndDocument{%
    \begin{frame}{Copyright Notice}
        \begin{columns}
            \begin{column}{0.7\textwidth}

                Individual authors may have certain copyright or licensing
                restrictions on their presentations. Please be certain to
                contact the original author to obtain permission to reuse or
                distribute these slides.
            \end{column}

            \begin{column}{0.3\textwidth}
            \end{column}
        \end{columns}
    \end{frame}
}

