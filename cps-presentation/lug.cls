\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{lug}[2017/10/18]

\LoadClass{beamer}

\usetheme[numbering=none,progressbar=frametitle,block=fill]{metropolis}
\setbeamercovered{dynamic}
\RequirePackage{graphicx}
\RequirePackage{animate}

\RequirePackage{ifxetex}
\ifxetex\RequirePackage{fontspec}\fi

\RequirePackage{minted}
\RequirePackage{xcolor}
\RequirePackage{hyperref}

\renewcommand*\footnoterule{}
\setminted{autogobble,python3,mathescape}

\beamertemplatenavigationsymbolsempty%
\def\logoimage{graphics/lug}

\setlength\parindent{0pt}

\AtBeginDocument{%
    \begin{frame}
        \maketitle
    \end{frame}
}

\AtEndDocument{%
    \begin{frame}

      \huge
        Questions?

    \end{frame}
}

