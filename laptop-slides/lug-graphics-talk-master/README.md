# Topics of Discussion:
* History of graphics in UNIX?
* What does a person need to know so that the rest of this makes sense?
* What is a GPU and why does it matter?
* How do graphical calls get from the application to the GPU?

* Why does X exist?
	* Why does Wayland exist?
	* What distros have Wayland?
	* How backwards-compatible is Wayland?

* What is Mesa?

* What causes sucky graphics performance?

* Why is Nvidia terrible?
	* Differences between AMD and Nvidia business model
	* Noveau vs proprietary driver (why do you need Noveau for Wayland?)
	* Why/when do we need Bumblebee?

* Are Intel graphics different?
* What is Gallium 9?
