#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#define NUMTHREADS 20


int counter = 0;

struct combinedData {
    int id;
    pthread_mutex_t *lock;
};

void *worker (void *arg) {
    struct combinedData *inputData = (struct combinedData *) arg;
    
    //pthread_mutex_lock(inputData->lock);
    for (int i = 0; i < 10000000; i++){
        //pthread_mutex_lock(inputData->lock);
        counter++;
        //pthread_mutex_unlock(inputData->lock);
    }
    //pthread_mutex_unlock(inputData->lock);

    printf("Thread %d is done\n", inputData->id);
    pthread_exit(NULL);
}

int main() {
    pthread_t threadArr[NUMTHREADS];
    pthread_mutex_t lock;

    pthread_mutex_init(&lock, NULL);

    for(int i = 0; i < NUMTHREADS; i++) {
        struct combinedData *threadInputData = malloc(sizeof(struct combinedData));
        
        threadInputData->id = i;
        threadInputData->lock = &lock;

        pthread_create(&threadArr[i], NULL, worker, (void *) threadInputData);
    }

    for(int i = 0; i<NUMTHREADS; i++) {
        pthread_join(threadArr[i], NULL);
    }

    printf("Final Count = %d\n", counter);
    return 0;
}
